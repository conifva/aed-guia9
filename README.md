# AED-Guia9

Actividades septima guía de AED 2019 (Segunda guía U3: Métodos de Búsqueda - Tablas Hash)


# Guia9-UIII

Tema: Métodos de Búsqueda - Tablas Hash

> Programa que permita el ingreso y busqueda de información númerica utilizando los siguientes métodos de resolución de colisiones.
> 
>	- Reasignación Prueba Lineal (L)
>	- Reasignación Prueba Cuadrática (C)
>	- Reasignación Doble Dirección Hash (D)
>	- Encadenamiento (E)
>
> Por cada ingreso: Imprimir el contenido del arreglo. En caso de ocurrir una colisión, indicar dónde y cual fue el desplazamiento final.
> 
> Por cada búsqueda: Indicar la posición donde se encuentra. En caso de ocurrir una colisión, indicar dónde y cual fue el desplazamiento final.
> 


# Obtención del Programa
Clonar repositorio, ingresar a la carpeta ej1 y ejecutar por terminal:
```
$ g++ main.cpp Busqueda.cpp -o hash
$ make
$ ./hash argumento2  
```
Donde argumento2 corresponde al método de resolución de colisiones.


# Acerca de 
Para iniciar el programa es necessario el ingreso de dos argumentos. En caso de no ingresarlos se notificará al usuario que reinicie el programa. 

El primero corresponde a ``` ./hash ```. Mientras que el segundo parametro ingresado indica el metodo de colisiones que desea usar, ya sea en mayuscula o en minuscula.

Una vez realizadas estas verificaciones, se informará al usuario el metodo selesccionado y solicitará el ingreso del largo del arreglo que se creará. Luego, se mostrará un menú con 3 opciones.

La primera opcion permite el ingreso de numeros al arreglo recientemente creado, se informará al usuario en caso de que ocurran colisiones y donde. La segunda alternativa corresponde a la busqueda de la informacion ingresada de acuerdo al metodo seleccionado. Finalmente la tercera opcion permite la salida o cierre del programa.


# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)


# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text


# Autor
Constanza Valenzuela